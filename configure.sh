#!/bin/bash
apt update -y
apt install default-jdk -y
apt install tomcat9 tomcat9-admin -y
ufw allow from any to any port 8080 proto tcp
ufw allow from any to any port 22 proto tcp
ufw allow from any to any port 80 proto tcp
ufw --force enable
systemctl enable tomcat9
systemctl restart tomcat9
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash
apt update
export GITLAB_RUNNER_DISABLE_SKEL=true; sudo -E apt-get install gitlab-runner
gitlab-runner register --url https://gitlab.com
gitlab-runner --debug start
usermod -a -G sudo gitlab-runner
echo "gitlab-runner ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers

